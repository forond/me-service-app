import axios from "axios";

export const cities = {
  namespaced: true,
  state: {
    data: null,
  },
  mutations: {
    set(state, value) {
      state.data = value;
    },
  },
  actions: {
    load({ commit }) {
      return axios
        .get("https://60254fac36244d001797bfe8.mockapi.io/api/v1/city")
        .then((response) => {
          commit("set", response.data);
        })
        .catch((error) => {
          commit("set", null);
          console.warn(error);
        });
    },
  },
};
