import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

import { cities } from "@/store/modules/cities.module";

export default new Vuex.Store({
  modules: {
    cities,
  },
});
